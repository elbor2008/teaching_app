import React, { Fragment, FC, useContext, useEffect } from 'react';
import Navbar from './components/nav/Navbar';
import { Route, Switch, RouteComponentProps, Redirect } from 'react-router-dom';
import Home from './components/home/Home';
import { Container } from 'semantic-ui-react';
import Course from './components/course/Course';
import SignIn from './components/auth/SignIn';
import SignUp from './components/auth/SignUp';
import NotFound from './components/notfound/NotFound';
import Classroom from './components/classroom/Classroom';
import ClassroomForm from './components/classroom/ClassroomForm';
import CourseForm from './components/course/CourseForm';
import AvailableCourseInfo from './components/home/AvailableCourseInfo';
import AssignCourse from './components/home/AssignCourse';
import RootStore from './stores/RootStore';
import { observer } from 'mobx-react-lite';
import DimLoader from './components/loader/DimLoader';
import ServerError from './components/notfound/ServerError';

interface Iprops {
  path: string | string[];
  component: Function;
}
const AuthRoute: FC<Iprops> = ({ path, component: Component }) => {
  const { userStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  return (
    <Route
      path={path}
      render={(props: RouteComponentProps) => {
        if (!selectedUser) {
          return (
            <Redirect
              to={{
                pathname: '/signIn',
                state: { from: props.location.pathname }
              }}
            />
          );
        }
        return <Component {...props} />;
      }}
    />
  );
};
const App: React.FC = () => {
  const { authStore } = useContext(RootStore);
  const { initializeUser, isLoading } = authStore;
  useEffect(() => {
    try {
      initializeUser();
    } catch (e) {}
  }, [initializeUser]);
  if (isLoading) {
    return <DimLoader />;
  }
  return (
    <Fragment>
      <Navbar />
      <Container style={{ marginTop: '7em' }}>
        <Switch>
          <Route exact path="/" component={Home} />
          <AuthRoute path="/courses" component={Course} />
          <AuthRoute
            path="/availableCourse/:id"
            component={AvailableCourseInfo}
          />
          <AuthRoute path="/assignCourse/:id" component={AssignCourse} />
          <AuthRoute
            path={['/createCourse', '/editCourse/:id']}
            component={CourseForm}
          />
          <AuthRoute path="/classrooms" component={Classroom} />
          <AuthRoute
            path={['/createClassroom', '/editClassroom/:id']}
            component={ClassroomForm}
          />
          <Route path="/signIn" component={SignIn} />
          <Route path="/signUp" component={SignUp} />
          <Route path="/error" component={ServerError} />
          <Route component={NotFound} />
        </Switch>
      </Container>
    </Fragment>
  );
};

export default observer(App);
