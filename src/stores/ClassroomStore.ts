import { IClassroom } from './../models/classroom';
import { observable, action, runInAction } from 'mobx';
import { ClassroomAgent } from '../api/agent';
import { RootStore } from './RootStore';

export class ClassroomStore {
  rootStore: RootStore;
  @observable classrooms: IClassroom[] = [];
  @observable selectedClassroom: IClassroom | undefined;
  @observable isSubmitting: boolean = false;
  @observable isLoading: boolean = false;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @action loadClassrooms = async () => {
    this.isLoading = true;
    try {
      const classrooms = await ClassroomAgent.list();
      runInAction('load classrooms', () => {
        this.classrooms = classrooms;
        this.isLoading = false;
      });
      return classrooms;
    } catch (error) {
      runInAction('load classrooms error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action loadClassroomsByTeacher = async () => {
    this.isLoading = true;
    try {
      const user = this.rootStore.userStore.selectedUser;
      const classrooms = await ClassroomAgent.listByTeacher(user!.id!);
      runInAction('load classrooms', () => {
        this.classrooms = classrooms;
        this.isLoading = false;
      });
      return classrooms;
    } catch (error) {
      runInAction('load classrooms error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action createClassroom = async (classroom: IClassroom) => {
    try {
      const isExisting = await ClassroomAgent.isNameExisting(classroom.name);
      if (!isExisting) {
        const user = this.rootStore.userStore.selectedUser;
        classroom.teacherId = user!.id;
        const createdClassroom = await ClassroomAgent.create(classroom);
        runInAction('add created classroom', () => {
          this.classrooms.push(createdClassroom);
        });
      }
    } catch (error) {
      runInAction('create classroom error', () => {
        console.log(error);
        throw error;
      });
    }
  };
  @action loadClassroom = async (id: string) => {
    this.isLoading = true;
    try {
      const classroom = this.classrooms.find(classroom => classroom.id === id);
      if (classroom) {
        this.selectedClassroom = classroom;
        this.isLoading = false;
        return classroom;
      } else {
        const classroom = await ClassroomAgent.detail(id);
        runInAction('load classroom', () => {
          this.selectedClassroom = classroom;
          this.isLoading = false;
        });
        return classroom;
      }
    } catch (error) {
      runInAction('load classroom error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action updateClassroom = async (classroom: IClassroom) => {
    try {
      const isExisting = await ClassroomAgent.isOtherNameExisting(
        classroom.id!,
        classroom.name
      );
      if (!isExisting) {
        const updatedClassroom = await ClassroomAgent.update(
          classroom.id!,
          classroom
        );
        runInAction('set classroom', () => {
          const index = this.classrooms.findIndex(
            classroom => classroom.id === updatedClassroom.id
          );
          this.classrooms[index] = updatedClassroom;
          this.selectedClassroom = updatedClassroom;
        });
      }
    } catch (error) {
      runInAction('update classroom error', () => {
        console.log(error);
        throw error;
      });
    }
  };
  @action deleteClassroom = async (id: string) => {
    this.isSubmitting = true;
    try {
      await ClassroomAgent.delete(id);
      const index = this.classrooms.findIndex(classroom => classroom.id === id);
      runInAction('delete classroom', () => {
        index >= 0 && this.classrooms.splice(index, 1);
        this.isSubmitting = false;
      });
    } catch (error) {
      runInAction('delete classroom error', () => {
        this.isSubmitting = false;
        console.log(error);
        throw error;
      });
    }
  };
}
