import { observable, action, runInAction } from 'mobx';
import { ClazzAgent } from '../api/agent';
import { RootStore } from './RootStore';
import { IClazz } from '../models/clazz';

export class ClazzStore {
  rootStore: RootStore;
  @observable selectedClazz: IClazz | undefined;
  @observable isSubmitting: boolean = false;
  @observable isLoading: boolean = false;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @action getDaysOfWeek = async (teacherId: string) => {
    this.isLoading = true;
    try {
      const daysOfWeek = await ClazzAgent.getDaysOfWeek(teacherId);
      runInAction('set loading state', () => {
        this.isLoading = false;
      });
      return daysOfWeek;
    } catch (error) {
      runInAction('get days of week error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action getClassroomsByDayOfWeek = async (dayOfWeek: string) => {
    this.isLoading = true;
    try {
      const classrooms = await ClazzAgent.getClassroomsByDayOfWeek(dayOfWeek);
      runInAction('set loading state', () => {
        this.isLoading = false;
      });
      return classrooms;
    } catch (error) {
      runInAction('get days of week error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action getClassroomsAndDaysOfWeek = async (
    studentId: string,
    courseId: string
  ) => {
    this.isLoading = true;
    try {
      const classroomsAndDaysOfWeek = await ClazzAgent.getClassroomsAndDaysOfWeek(
        studentId,
        courseId
      );
      runInAction('set loading state', () => {
        this.isLoading = false;
      });
      return classroomsAndDaysOfWeek;
    } catch (error) {
      runInAction('get classrooms and days of week error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action createClazz = async (clazz: IClazz) => {
    this.isSubmitting = true;
    try {
      const createdClazz = await ClazzAgent.create(clazz);
      runInAction('set submitting state', () => {
        this.isSubmitting = false;
      });
      return createdClazz;
    } catch (error) {
      runInAction('create clazz error', () => {
        this.isSubmitting = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action addStudent = async (clazzId: string, studentId: string) => {
    this.isSubmitting = true;
    try {
      await ClazzAgent.addStudent(clazzId, studentId);
      runInAction('set submitting state', () => {
        this.isSubmitting = false;
      });
    } catch (error) {
      runInAction('add student error', () => {
        this.isSubmitting = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action removeStudent = async (clazzId: string, studentId: string) => {
    this.isSubmitting = true;
    try {
      await ClazzAgent.removeStudent(clazzId, studentId);
      runInAction('set submitting state', () => {
        this.isSubmitting = false;
      });
    } catch (error) {
      runInAction('remove student error', () => {
        this.isSubmitting = false;
        console.log(error);
        throw error;
      });
    }
  };
}
