import { observable, action, runInAction, reaction } from 'mobx';
import jwt from 'jsonwebtoken';
import { AuthAgent } from '../api/agent';
import { RootStore } from './RootStore';
import { history } from '..';

export class AuthStore {
  rootStore: RootStore;
  @observable token = localStorage.getItem('jwt');
  @observable isLoading = true;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  tokenReaction = reaction(
    () => this.token,
    token => {
      if (token) {
        localStorage.setItem('jwt', token);
      } else {
        localStorage.removeItem('jwt');
      }
    }
  );
  @action clearUser = () => {
    this.rootStore.userStore.selectedUser = undefined;
    this.token = null;
  };
  @action initializeUser = async () => {
    if (!this.token) {
      this.isLoading = false;
      return;
    }
    const decoded: any = jwt.decode(this.token);
    if (!decoded || !decoded.name || !decoded.exp) {
      this.isLoading = false;
      return;
    }
    const expTime = decoded.exp * 1000;
    if (Date.now() >= expTime) {
      this.clearUser();
      this.isLoading = false;
      history.push('/signIn');
      return;
    }
    try {
      const res = await AuthAgent.signInByName(decoded.name);
      if (!res) {
        runInAction('set loading state', () => {
          this.isLoading = false;
        });
        return;
      }
      const { user, token } = res;
      runInAction('sign in user', () => {
        this.rootStore.userStore.selectedUser = user;
        this.token = token;
        this.isLoading = false;
      });
    } catch (error) {
      runInAction('sign in user error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };

  @action signInUser = async (email: string, password: string) => {
    try {
      const { user, token } = await AuthAgent.signIn(email, password);
      runInAction('sign in user', () => {
        this.rootStore.userStore.selectedUser = user;
        this.token = token;
      });
    } catch (error) {
      runInAction('sign in user error', () => {
        console.log(error);
        throw error;
      });
    }
  };
}
