import { observable, action, runInAction, computed } from 'mobx';
import { IUser } from '../models/user';
import { UserAgent } from '../api/agent';
import { RootStore } from './RootStore';

export class UserStore {
  rootStore: RootStore;
  @observable selectedUser: IUser | undefined;
  @observable isSubmitting: boolean = false;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @computed get isSignedIn() {
    return !!this.selectedUser;
  }
  @action createUser = async (user: IUser) => {
    try {
      const isExisting = await UserAgent.isEmailExisting(user.email);
      if (!isExisting) {
        await UserAgent.create(user);
      }
    } catch (error) {
      runInAction('create user error', () => {
        console.log(error);
        throw error;
      });
    }
  };
  @action updateUser = async (user: IUser) => {
    try {
      const isExisting = await UserAgent.isOtherEmailExisting(
        user.id!,
        user.email
      );
      if (!isExisting) {
        const updatedUser = await UserAgent.update(user.id!, user);
        runInAction('set user', () => {
          this.selectedUser = updatedUser;
        });
      }
    } catch (error) {
      runInAction('update user error', () => {
        console.log(error);
        throw error;
      });
    }
  };
}
