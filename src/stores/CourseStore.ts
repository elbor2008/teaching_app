import { observable, action, runInAction } from 'mobx';
import { CourseAgent } from '../api/agent';
import { ICourse } from '../models/course';
import { RootStore } from './RootStore';
import { Role } from '../models/role';

export class CourseStore {
  rootStore: RootStore;
  @observable courses: ICourse[] = [];
  @observable selectedCourse: ICourse | undefined;
  @observable isLoading = false;
  @observable isLoadingMore = false;
  @observable isSubmitting = false;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @action loadMoreAvailableCourses = async (page: number) => {
    this.isLoadingMore = true;
    try {
      const user = this.rootStore.userStore.selectedUser;
      if (!user || user.role === Role.Student) {
        const { courses, hasMore } = await CourseAgent.listMoreWithClazzs(page);
        runInAction('load courses', () => {
          this.courses = courses;
          this.isLoadingMore = false;
        });
        return { courses, hasMore };
      } else {
        const { courses, hasMore } = await CourseAgent.listMoreByTeacher(
          user.id!,
          page
        );
        runInAction('load courses', () => {
          this.courses = courses;
          this.isLoadingMore = false;
        });
        return { courses, hasMore };
      }
    } catch (error) {
      runInAction('load courses error', () => {
        this.isLoadingMore = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action loadAvailableCourses = async () => {
    this.isLoading = true;
    try {
      const user = this.rootStore.userStore.selectedUser;
      if (!user || user.role === Role.Student) {
        const { courses, hasMore } = await CourseAgent.listWithClazzs();
        runInAction('load courses', () => {
          this.courses = courses;
          this.isLoading = false;
        });
        return { courses, hasMore };
      } else {
        const { courses, hasMore } = await CourseAgent.listByTeacher(user.id!);
        runInAction('load courses', () => {
          this.courses = courses;
          this.isLoading = false;
        });
        return { courses, hasMore };
      }
    } catch (error) {
      runInAction('load courses error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action createCourse = async (course: ICourse) => {
    try {
      const isExisting = await CourseAgent.isNameExisting(course.name);
      if (!isExisting) {
        const isExisting = await CourseAgent.isCourseCodeExisting(
          course.courseCode
        );
        if (!isExisting) {
          const createdCourse = await CourseAgent.create(course);
          runInAction('add created course', () => {
            this.courses.push(createdCourse);
          });
        }
      }
    } catch (error) {
      runInAction('create course error', () => {
        console.log(error);
        throw error;
      });
    }
  };
  @action addClazz = async (id: string, clazzId: string) => {
    this.isSubmitting = true;
    try {
      await CourseAgent.addClazz(id, clazzId);
      runInAction('set submitting state', () => {
        this.isSubmitting = false;
      });
    } catch (error) {
      runInAction('add clazz to course error', () => {
        this.isSubmitting = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action updateCourse = async (course: ICourse) => {
    try {
      const isExisting = await CourseAgent.isOtherNameExisting(
        course.id!,
        course.name
      );
      if (!isExisting) {
        const isExisting = await CourseAgent.isOtherCourseCodeExisting(
          course.id!,
          course.courseCode
        );
        if (!isExisting) {
          const updatedCourse = await CourseAgent.update(course.id!, course);
          runInAction('set course', () => {
            const index = this.courses.findIndex(
              course => course.id === updatedCourse.id
            );
            this.courses[index] = updatedCourse;
            this.selectedCourse = updatedCourse;
          });
        }
      }
    } catch (error) {
      runInAction('update course error', () => {
        console.log(error);
        throw error;
      });
    }
  };
  @action loadCourse = async (id: string) => {
    this.isLoading = true;
    try {
      const course = await CourseAgent.detail(id);
      runInAction('load course', () => {
        course!.commenceDate = new Date(course!.commenceDate);
        course!.completeDate = new Date(course!.completeDate);
        this.selectedCourse = course;
        this.isLoading = false;
      });
      return course;
    } catch (error) {
      runInAction('load course error', () => {
        this.isLoading = false;
        console.log(error);
        throw error;
      });
    }
  };
  @action deleteCourse = async (id: string) => {
    this.isSubmitting = true;
    try {
      await CourseAgent.delete(id);
      const index = this.courses.findIndex(course => course.id === id);
      runInAction('delete course', () => {
        index >= 0 && this.courses.splice(index, 1);
        this.isSubmitting = false;
      });
    } catch (error) {
      runInAction('delete course error', () => {
        this.isSubmitting = false;
        console.log(error);
        throw error;
      });
    }
  };
}
