import { createContext } from 'react';
import { UserStore } from './UserStore';
import { CourseStore } from './CourseStore';
import { ClassroomStore } from './ClassroomStore';
import { ClazzStore } from './ClazzStore';
import { configure } from 'mobx';
import { AuthStore } from './AuthStore';

configure({ enforceActions: 'observed' });

export class RootStore {
  authStore: AuthStore;
  userStore: UserStore;
  courseStore: CourseStore;
  classroomStore: ClassroomStore;
  clazzStore: ClazzStore;
  constructor() {
    this.authStore = new AuthStore(this);
    this.userStore = new UserStore(this);
    this.courseStore = new CourseStore(this);
    this.classroomStore = new ClassroomStore(this);
    this.clazzStore = new ClazzStore(this);
  }
}

export default createContext(new RootStore());
