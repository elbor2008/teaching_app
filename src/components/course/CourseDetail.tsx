import React, { FC } from 'react';
import { ICourse } from '../../models/course';
import { Card, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { formatDate } from '../../utils/dateUtil';

interface Iprops {
  course: ICourse;
  handleDelete: (id: string) => void;
  isSubmitting: boolean;
}
const CourseDetail: FC<Iprops> = ({ course, handleDelete, isSubmitting }) => {
  return (
    <Card>
      <Card.Content>
        <Card.Header>{course.courseCode}</Card.Header>
        <Card.Meta>{course.name}</Card.Meta>
        <Card.Description>{course.objective}</Card.Description>
        <Card.Content>
          {formatDate(course.commenceDate)} - {formatDate(course.completeDate)}
        </Card.Content>
      </Card.Content>
      <Card.Content extra>
        <div className="ui two buttons">
          <Button as={Link} to={`/editCourse/${course.id}`} basic color="teal">
            Edit
          </Button>
          <Button
            onClick={() => handleDelete(course.id!)}
            basic
            color="red"
            loading={isSubmitting}
          >
            Delete
          </Button>
        </div>
      </Card.Content>
    </Card>
  );
};

export default CourseDetail;
