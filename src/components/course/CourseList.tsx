import React, { useContext, useEffect, Fragment, useState, FC } from 'react';
import { observer } from 'mobx-react-lite';
import { Grid, Card, Confirm, Loader, Button } from 'semantic-ui-react';
import CourseDetail from './CourseDetail';
import RootStore from '../../stores/RootStore';
import { ICourse } from '../../models/course';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import DimLoader from '../loader/DimLoader';

const CourseList: FC<RouteComponentProps> = ({ history }) => {
  const { courseStore } = useContext(RootStore);
  const {
    loadAvailableCourses,
    isSubmitting,
    deleteCourse,
    loadMoreAvailableCourses,
    isLoading,
    isLoadingMore
  } = courseStore;
  const [isOpen, setOpen] = useState(false);
  const [deleteId, setDeleteId] = useState('');
  const [courses, setCourses] = useState<ICourse[]>([]);
  const [hasMore, setHasMore] = useState(false);
  const [page, setPage] = useState(1);
  const handleDelete = (id: string) => {
    setDeleteId(id);
    setOpen(true);
  };
  const handleConfirm = async () => {
    await deleteCourse(deleteId);
    setOpen(false);
    history.push('/');
  };
  const loadMoreData = async () => {
    const res = await loadMoreAvailableCourses(page);
    if (res) {
      setHasMore(res.hasMore);
      setCourses(courses.concat(res.courses));
      setPage(page + 1);
    }
  };
  useEffect(() => {
    loadAvailableCourses().then(
      (
        res:
          | {
              courses: ICourse[];
              hasMore: boolean;
            }
          | undefined
      ) => {
        if (res) {
          setCourses(res.courses);
          setHasMore(res.hasMore);
        }
      }
    );
  }, [loadAvailableCourses]);
  if (isLoading) {
    return <DimLoader />;
  }
  return (
    <Fragment>
      <Grid textAlign="center">
        <Grid.Column>
          <Card.Group stackable>
            {courses.map(course => (
              <CourseDetail
                key={course.id}
                course={course}
                handleDelete={handleDelete}
                isSubmitting={isSubmitting}
              />
            ))}
          </Card.Group>
          {hasMore &&
            (isLoadingMore ? (
              <Loader content="Loading" active inline="centered" />
            ) : (
              <Button
                style={{ marginTop: 10 }}
                floated="left"
                color="teal"
                onClick={loadMoreData}
              >
                Load More ...
              </Button>
            ))}
        </Grid.Column>
      </Grid>
      <Confirm
        open={isOpen}
        onCancel={() => setOpen(false)}
        onConfirm={() => handleConfirm()}
      />
    </Fragment>
  );
};

export default withRouter(observer(CourseList));
