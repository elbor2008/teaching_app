import React, {
  useContext,
  useState,
  FC,
  useEffect,
  Fragment,
  useRef
} from 'react';
import { Form, Button, Label, Select } from 'semantic-ui-react';
import { Form as FinalForm, Field } from 'react-final-form';
import { ICourse } from '../../models/course';
import { Method } from '../../models/method';
import { Length } from '../../models/length';
import { Level } from '../../models/level';
import { Tuition } from '../../models/tuition';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router';
import TextInput from '../custom/TextInput';
import SelectInput from '../custom/SelectInput';
import TextAreaInput from '../custom/TextAreaInput';
import { Link } from 'react-router-dom';
import RootStore from '../../stores/RootStore';
import { FORM_ERROR } from 'final-form';
import { combineValidators, isRequired } from 'revalidate';
import { addWeeks } from '../../utils/dateUtil';
import DatePicker from 'react-datepicker';

const lengthOptions = [
  { key: 1, text: '4 weeks', value: '4' },
  { key: 2, text: '13 weeks', value: '13' }
];
const levelOptions = [
  { key: 1, text: 'entry', value: 'entry' },
  { key: 2, text: 'intermediate', value: 'intermediate' },
  { key: 3, text: 'advanced', value: 'advanced' }
];
const methodOptions = [
  { key: 1, text: 'lecture', value: 'lecture' },
  { key: 2, text: 'turorial', value: 'turorial' },
  { key: 3, text: 'workshop', value: 'workshop' }
];
const tuitionOptions = [
  { key: 1, text: '$1000', value: '1000' },
  { key: 2, text: '$2000', value: '2000' },
  { key: 3, text: '$3000', value: '3000' }
];
const validator = combineValidators({
  name: isRequired('name'),
  courseCode: isRequired('course code')
});
const CourseForm: FC<RouteComponentProps<{ id?: string }>> = ({
  history,
  match
}) => {
  const completeDateRef = useRef<any>(null);
  const { courseStore, userStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  const { isLoading, createCourse, updateCourse, loadCourse } = courseStore;

  const [course, setCourse] = useState<ICourse>({
    name: '',
    courseCode: '',
    prerequisite: '',
    method: Method.lecture,
    length: Length.short,
    objective: '',
    level: Level.entry,
    commenceDate: new Date(),
    completeDate: addWeeks(new Date(), parseInt(Length.short)),
    tuition: Tuition.low,
    teacher: selectedUser!.id!
  });
  const handleFormSubmit = async (course: ICourse) => {
    try {
      if (!course.id) {
        await createCourse(course);
      } else {
        await updateCourse(course);
      }
      history.push('/courses');
    } catch (e) {
      return { [FORM_ERROR]: e.data.message };
    }
  };
  useEffect(() => {
    if (match.params.id) {
      loadCourse(match.params.id).then(course => {
        course && setCourse(course);
      });
    }
  }, [loadCourse, match.params.id]);
  return (
    <FinalForm
      validate={validator}
      initialValues={course}
      onSubmit={handleFormSubmit}
      render={({
        handleSubmit,
        pristine,
        submitting,
        submitError,
        dirtySinceLastSubmit,
        hasValidationErrors,
        values
      }) => (
        <Form
          className="fluid segment"
          onSubmit={handleSubmit}
          loading={isLoading}
        >
          <Form.Group widths={3}>
            <Field
              name="name"
              label="NAME"
              type="text"
              placeholder="Name"
              component={TextInput}
            />
            <Field
              name="courseCode"
              label="COURSE CODE"
              type="text"
              placeholder="Course Code"
              component={TextInput}
            />
            <Field
              name="prerequisite"
              label="PREREQUISITE"
              type="text"
              placeholder="Prerequisite"
              component={TextInput}
            />
          </Form.Group>
          <Form.Group widths={3}>
            <Field
              name="method"
              label="METHOD"
              options={methodOptions}
              component={SelectInput}
            />
            <Field
              name="length"
              render={({ input }) => (
                <Form.Field>
                  <label>LENGTH</label>
                  <Select
                    options={lengthOptions}
                    value={input.value}
                    onChange={(e, data) => {
                      input.onChange(data.value);
                      completeDateRef.current.setSelected(
                        addWeeks(
                          values.commenceDate,
                          parseInt(data.value as string)
                        )
                      );
                    }}
                  />
                </Form.Field>
              )}
            />
            <Field
              name="level"
              label="LEVEL"
              options={levelOptions}
              component={SelectInput}
            />
          </Form.Group>
          <Form.Group widths={3}>
            <Field
              name="tuition"
              label="TUITION"
              options={tuitionOptions}
              component={SelectInput}
            />
            <Form.Field>
              <Field
                name="commenceDate"
                render={({ input }) => (
                  <Fragment>
                    <label>COMMENCE DATE</label>
                    <DatePicker
                      selected={input.value}
                      minDate={course.commenceDate}
                      onChange={data => {
                        input.onChange(data);
                        completeDateRef.current.setSelected(
                          addWeeks(data!, parseInt(values.length))
                        );
                      }}
                      withPortal
                      dateFormat="dd/MM/yyyy"
                    />
                  </Fragment>
                )}
              />
            </Form.Field>
            <Form.Field>
              <Field
                name="completeDate"
                render={({ input }) => (
                  <Fragment>
                    <label>COMPLETE DATE</label>
                    <DatePicker
                      ref={completeDateRef}
                      selected={input.value}
                      onChange={input.onChange}
                      dateFormat="dd/MM/yyyy"
                      withPortal
                      readOnly
                    />
                  </Fragment>
                )}
              />
            </Form.Field>
          </Form.Group>
          <Form.Group grouped>
            <Field
              name="objective"
              label="OBJECTIVE"
              placeholder="Objective"
              component={TextAreaInput}
              rows={2}
            />
          </Form.Group>
          <Button
            loading={submitting}
            type="submit"
            color="teal"
            disabled={
              pristine ||
              submitting ||
              hasValidationErrors ||
              (submitError && !dirtySinceLastSubmit)
            }
          >
            Submit
          </Button>
          <Button as={Link} to="/courses">
            Cancel
          </Button>
          {submitError && !dirtySinceLastSubmit && (
            <Label basic color="red" content={submitError} />
          )}
        </Form>
      )}
    />
  );
};

export default observer(CourseForm);
