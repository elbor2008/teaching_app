import React, { Fragment, useContext } from 'react';
import { Button } from 'semantic-ui-react';
import { Link, Redirect } from 'react-router-dom';
import CourseList from './CourseList';
import RootStore from '../../stores/RootStore';
import { Role } from '../../models/role';

const Course = () => {
  const { userStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  if (selectedUser && selectedUser.role === Role.Student) {
    return <Redirect to="/notfound" />;
  }
  return (
    <Fragment>
      <p>
        <Button as={Link} to="/createCourse" color="teal">
          Create Course
        </Button>
      </p>
      <CourseList />
    </Fragment>
  );
};

export default Course;
