import React from 'react';
import { Segment, Header, Icon } from 'semantic-ui-react';

const ServerError = () => {
  return (
    <Segment placeholder>
      <Header icon>
        <Icon name="warning" />
        Oops, server is currently not working.
      </Header>
    </Segment>
  );
};

export default ServerError;
