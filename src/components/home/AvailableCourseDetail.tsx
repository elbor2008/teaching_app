import React, { FC, useContext } from 'react';
import { ICourse } from '../../models/course';
import { Card, Button, Label, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { IClazz } from '../../models/clazz';
import { getDayOfWeek } from '../../utils/dateUtil';
import { IClassroom } from '../../models/classroom';
import RootStore from '../../stores/RootStore';
import { Role } from '../../models/role';
import { observer } from 'mobx-react-lite';
import { IUser } from '../../models/user';

interface Iprops {
  course: ICourse;
}
const AvailableCourseDetail: FC<Iprops> = ({ course }) => {
  const { userStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  const clazz = course.clazz as IClazz;
  const isEnrolled = !!(
    selectedUser &&
    selectedUser.role === Role.Student &&
    course.clazz &&
    (clazz.students! as string[]).includes(selectedUser.id!)
  );
  const isAssigned = !!(
    selectedUser &&
    selectedUser.role !== Role.Student &&
    course.clazz
  );
  return (
    <Card>
      <Card.Content>
        {isEnrolled ? (
          <Label color="teal" ribbon content="Enrolled" />
        ) : isAssigned ? (
          <Label color="teal" ribbon content="Assigned" />
        ) : (
          <Label ribbon content="a" style={{ visibility: 'hidden' }} />
        )}
        <Card.Header textAlign="center">{course.courseCode}</Card.Header>
        <Card.Meta textAlign="center">{course.name}</Card.Meta>
        {clazz && (
          <Card.Description textAlign="center">
            <Icon name="time" />
            {'Room ' +
              (clazz.classroom as IClassroom).name +
              ' on ' +
              getDayOfWeek(clazz.dayOfWeek) +
              (!selectedUser || selectedUser.role === Role.Student
                ? ' by ' + (course.teacher as IUser).name
                : '')}
          </Card.Description>
        )}
        {clazz && (
          <Card.Description textAlign="center" style={{ marginTop: 2 }}>
            <Icon name="user" />
            {clazz.students!.length
              ? clazz.students!.length + ' students enrolled'
              : '0 student enrolled'}
          </Card.Description>
        )}
      </Card.Content>
      <Card.Content extra>
        <Button
          as={Link}
          to={`/availableCourse/${course.id}`}
          basic
          color="teal"
          fluid
        >
          View
        </Button>
      </Card.Content>
    </Card>
  );
};

export default observer(AvailableCourseDetail);
