import React, { FC, useEffect, useContext, useState, Fragment } from 'react';
import { RouteComponentProps } from 'react-router';
import RootStore from '../../stores/RootStore';
import { Form, Button, Message, Label } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import { ICourse } from '../../models/course';
import { Method } from '../../models/method';
import { Length } from '../../models/length';
import { Level } from '../../models/level';
import { Tuition } from '../../models/tuition';
import { formatDate } from '../../utils/dateUtil';
import { Link } from 'react-router-dom';
import { Role } from '../../models/role';
import { IClazz } from '../../models/clazz';

const AvailableCourseInfo: FC<RouteComponentProps<{ id?: string }>> = ({
  match,
  history
}) => {
  const { courseStore, userStore, clazzStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  const { loadCourse, isLoading } = courseStore;
  const { addStudent, removeStudent, isSubmitting } = clazzStore;
  const [errorMessage, setErrorMessage] = useState('');
  const [course, setCourse] = useState<ICourse>({
    name: '',
    courseCode: '',
    prerequisite: '',
    method: Method.lecture,
    length: Length.short,
    objective: '',
    level: Level.entry,
    commenceDate: new Date(),
    completeDate: new Date(),
    tuition: Tuition.low,
    teacher: selectedUser!.id!
  });
  const handleEnrollment = async () => {
    try {
      await addStudent((course.clazz as IClazz).id!, selectedUser!.id!);
      history.push('/');
    } catch (e) {
      setErrorMessage(e.data.message);
    }
  };
  const handleWithdraw = async () => {
    try {
      await removeStudent((course.clazz as IClazz).id!, selectedUser!.id!);
      history.push('/');
    } catch (e) {}
  };
  useEffect(() => {
    if (match.params.id && !course.id) {
      loadCourse(match.params.id).then(course => {
        course && setCourse(course);
      });
    }
  }, [loadCourse, match.params.id, course.id]);
  return (
    <Fragment>
      {selectedUser &&
        selectedUser.role === Role.Student &&
        course.clazz &&
        ((course.clazz as IClazz).students! as string[]).includes(
          selectedUser.id!
        ) && <Message attached header="Your have enrolled this course!" />}
      {selectedUser && selectedUser.role !== Role.Student && course.clazz && (
        <Message attached header="Your have assigned this course!" />
      )}
      <Form className="fluid segment" loading={isLoading}>
        <Form.Group widths={3}>
          <Form.Input name="name" label="NAME" value={course.name} readOnly />
          <Form.Input
            name="courseCode"
            label="COURSE CODE"
            value={course.courseCode}
            readOnly
          />
          <Form.Input
            name="prerequisite"
            label="PREREQUISITE"
            value={course.prerequisite}
            readOnly
          />
        </Form.Group>
        <Form.Group widths={3}>
          <Form.Input
            name="method"
            label="METHOD"
            value={course.method}
            readOnly
          />
          <Form.Input
            name="length"
            label="LENGTH"
            value={course.length + ' weeks'}
            readOnly
          />
          <Form.Input
            name="level"
            label="LEVEL"
            value={course.level}
            readOnly
          />
        </Form.Group>
        <Form.Group widths={3}>
          <Form.Input
            name="tuition"
            label="TUITION"
            value={'$' + course.tuition}
            readOnly
          />
          <Form.Input
            name="commenceDate"
            label="COMMENCE DATE"
            value={formatDate(course.commenceDate)}
            readOnly
          />
          <Form.Input
            name="completeDate"
            label="COMPLETE DATE"
            value={formatDate(course.completeDate)}
            readOnly
          />
        </Form.Group>
        <Form.Group grouped>
          <Form.TextArea
            name="objective"
            label="OBJECTIVE"
            rows={2}
            value={course.objective}
            readOnly
          />
        </Form.Group>
        {selectedUser &&
          selectedUser.role === Role.Student &&
          (course.clazz &&
          ((course.clazz as IClazz).students! as string[]).includes(
            selectedUser.id!
          ) ? (
            <Button
              onClick={handleWithdraw}
              color="red"
              content="Withdraw"
              loading={isSubmitting}
              disabled={isSubmitting}
            />
          ) : (
            <Button
              onClick={handleEnrollment}
              color="teal"
              content="Enroll"
              loading={isSubmitting}
              disabled={isSubmitting}
            />
          ))}
        {selectedUser && selectedUser.role !== Role.Student && (
          <Button
            as={Link}
            to={`/assignCourse/${course.id}`}
            content="Assign"
            color="teal"
            disabled={!!course.clazz}
          />
        )}
        <Button as={Link} to="/" content="Cancel" />
        {errorMessage && <Label basic color="red" content={errorMessage} />}
      </Form>
    </Fragment>
  );
};

export default observer(AvailableCourseInfo);
