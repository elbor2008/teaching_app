import React, { useContext, useEffect, useState } from 'react';
import RootStore from '../../stores/RootStore';
import { Grid, Card, Button, Loader } from 'semantic-ui-react';
import AvailableCourseDetail from './AvailableCourseDetail';
import { observer } from 'mobx-react-lite';
import { ICourse } from '../../models/course';
import DimLoader from '../loader/DimLoader';

const AvailableCourseList = () => {
  const { courseStore } = useContext(RootStore);
  const {
    loadAvailableCourses,
    loadMoreAvailableCourses,
    isLoading,
    isLoadingMore
  } = courseStore;
  const [courses, setCourses] = useState<ICourse[]>([]);
  const [hasMore, setHasMore] = useState(false);
  const [page, setPage] = useState(1);
  const loadMoreData = async () => {
    const res = await loadMoreAvailableCourses(page);
    if (res) {
      setHasMore(res.hasMore);
      setCourses(courses.concat(res.courses));
      setPage(page + 1);
    }
  };
  useEffect(() => {
    loadAvailableCourses().then(
      (
        res:
          | {
              courses: ICourse[];
              hasMore: boolean;
            }
          | undefined
      ) => {
        if (res) {
          setCourses(res.courses);
          setHasMore(res.hasMore);
        }
      }
    );
  }, [loadAvailableCourses]);
  if (isLoading) {
    return <DimLoader />;
  }
  return (
    <Grid>
      <Grid.Column>
        <Card.Group stackable>
          {courses.map(course => (
            <AvailableCourseDetail key={course.id} course={course} />
          ))}
        </Card.Group>
        {hasMore &&
          (isLoadingMore ? (
            <Loader content="Loading" active inline="centered" />
          ) : (
            <Button
              style={{ marginTop: 10 }}
              color="teal"
              onClick={loadMoreData}
            >
              Load More ...
            </Button>
          ))}
      </Grid.Column>
    </Grid>
  );
};

export default observer(AvailableCourseList);
