import React, { useState, useEffect, useContext, FC } from 'react';
import { Form, Select, DropdownProps, Button } from 'semantic-ui-react';
import RootStore from '../../stores/RootStore';
import { RouteComponentProps } from 'react-router';
import { ICourse } from '../../models/course';
import { IClazz } from '../../models/clazz';
import { observer } from 'mobx-react-lite';

const initialDaysOfWeek = [
  { key: '1', text: 'Monday', value: '1' },
  { key: '2', text: 'Tuesday', value: '2' },
  { key: '3', text: 'Wednesday', value: '3' },
  { key: '4', text: 'Thursday', value: '4' },
  { key: '5', text: 'Friday', value: '5' }
];
const AssignCourse: FC<RouteComponentProps<{ id?: string }>> = ({
  match,
  history
}) => {
  const { classroomStore, clazzStore, courseStore, userStore } = useContext(
    RootStore
  );
  const [course, setCourse] = useState<ICourse | null>(null);
  const [showClassroomSelect, setShowClassroomSelect] = useState(false);
  const [dayOfWeek, setDayOfWeek] = useState<string>();
  const [showAssignButton, setShowAssignButton] = useState(false);
  const [classroomId, setClassroomId] = useState<string>();
  const [daysOfWeekOptions, setDaysOfWeekOptions] = useState<
    { [key: string]: string }[]
  >([]);
  const [classrooms, setClassrooms] = useState<{ [key: string]: string }[]>([]);
  const [availableClassrooms, setAvailableClassrooms] = useState<
    { [key: string]: string }[]
  >([]);
  const { selectedUser } = userStore;
  const { loadCourse, addClazz } = courseStore;
  const isCourseSubmitting = courseStore.isSubmitting;
  const { createClazz, getDaysOfWeek, getClassroomsByDayOfWeek } = clazzStore;
  const isClazzSubmitting = clazzStore.isSubmitting;
  const { loadClassrooms } = classroomStore;
  const isDaysOfWeekLoading = clazzStore.isLoading;

  const handleSelectDayOfWeek = async (
    event: React.SyntheticEvent<HTMLElement, Event>,
    data: DropdownProps
  ) => {
    setShowAssignButton(false);
    setClassroomId('');
    setDayOfWeek(data.value as string);
    const classroomIds = await getClassroomsByDayOfWeek(data.value as string);
    setAvailableClassrooms(
      classrooms.filter(classroom => !classroomIds!.includes(classroom.value))
    );
    setShowClassroomSelect(true);
  };
  const handleSelectClassroom = async (
    event: React.SyntheticEvent<HTMLElement, Event>,
    data: DropdownProps
  ) => {
    setClassroomId(data.value as string);
    setShowAssignButton(true);
  };
  const handleAssign = async () => {
    try {
      const clazz: IClazz = {
        dayOfWeek: dayOfWeek!,
        course: course!.id!,
        teacher: selectedUser!.id!,
        classroom: classroomId!
      };
      const createdClazz = await createClazz(clazz);
      await addClazz(course!.id!, createdClazz!.id!);
      history.push('/');
    } catch (e) {}
  };
  useEffect(() => {
    if (match.params.id && !course) {
      loadCourse(match.params.id).then(course => {
        course && setCourse(course);
        getDaysOfWeek(selectedUser!.id!).then(daysOfWeek => {
          setDaysOfWeekOptions(
            initialDaysOfWeek.filter(day => !daysOfWeek!.includes(day.value))
          );
        });
        loadClassrooms().then(classrooms => {
          if (classrooms && classrooms.length > 0) {
            const classroomsArray = classrooms.map(classroom => ({
              key: classroom.id!,
              text: classroom.name!,
              value: classroom.id!
            }));
            setClassrooms(classroomsArray);
          }
        });
      });
    }
  }, [
    loadClassrooms,
    loadCourse,
    course,
    match.params.id,
    getDaysOfWeek,
    selectedUser
  ]);
  return (
    <Form className="fluid segment">
      <Form.Group widths={3}>
        <Form.Field>
          <label>AVAILABLE ASSIGNMENT DAY</label>
          <Select
            loading={isDaysOfWeekLoading}
            placeholder={
              daysOfWeekOptions.length > 0
                ? 'Choose assignment day'
                : 'No assignment day available'
            }
            options={daysOfWeekOptions}
            onChange={handleSelectDayOfWeek}
          />
        </Form.Field>
        {showClassroomSelect && (
          <Form.Field>
            <label>AVAILABLE CLASSROOMS</label>
            <Select
              loading={isDaysOfWeekLoading}
              placeholder={
                availableClassrooms.length > 0
                  ? 'Choose classroom'
                  : 'No classroom available'
              }
              options={availableClassrooms}
              onChange={handleSelectClassroom}
              value={classroomId}
            />
          </Form.Field>
        )}
      </Form.Group>
      {showAssignButton && (
        <Button
          content="Assign"
          positive
          onClick={handleAssign}
          disabled={isClazzSubmitting || isCourseSubmitting}
          loading={isClazzSubmitting || isCourseSubmitting}
        />
      )}
      <Button content="Cancel" onClick={() => history.goBack()} />
    </Form>
  );
};

export default observer(AssignCourse);
