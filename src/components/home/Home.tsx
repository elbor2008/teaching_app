import React, { Fragment, useContext } from 'react';
import { Message } from 'semantic-ui-react';
import AvailableCourseList from './AvailableCourseList';
import RootStore from '../../stores/RootStore';
import { Role } from '../../models/role';

const Home = () => {
  const { userStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  return (
    <Fragment>
      <Message
        attached
        header={
          !selectedUser || selectedUser!.role === Role.Student
            ? 'Available Courses'
            : 'Your Courses'
        }
        content={
          'Feel free to ' +
          (!selectedUser || selectedUser!.role === Role.Student
            ? 'enroll the course you fancy!'
            : 'assign the course you created!')
        }
      />
      <br />
      <AvailableCourseList />
    </Fragment>
  );
};

export default Home;
