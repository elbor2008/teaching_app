import React, { Fragment, useContext } from 'react';
import { Button } from 'semantic-ui-react';
import { Link, Redirect } from 'react-router-dom';
import ClassroomList from './ClassroomList';
import RootStore from '../../stores/RootStore';
import { Role } from '../../models/role';

const Classroom = () => {
  const { userStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  if (selectedUser && selectedUser.role === Role.Student) {
    return <Redirect to="/notfound" />;
  }
  return (
    <Fragment>
      <p>
        <Button as={Link} to="/createClassroom" color="teal">
          Create Classroom
        </Button>
      </p>
      <ClassroomList />
    </Fragment>
  );
};

export default Classroom;
