import React, { FC } from 'react';
import { Card, Button } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import { IClassroom } from '../../models/classroom';
import { Link } from 'react-router-dom';

interface Iprops {
  classroom: IClassroom;
  handleDelete: (id: string) => void;
  isSubmitting: boolean;
}
const ClassroomDetail: FC<Iprops> = ({
  classroom,
  handleDelete,
  isSubmitting
}) => {
  return (
    <Card>
      <Card.Content>
        <Card.Header>Classroom No.</Card.Header>
        <Card.Meta>{classroom.name}</Card.Meta>
      </Card.Content>
      <Card.Content extra>
        <div className="ui two buttons">
          <Button
            as={Link}
            to={`/editClassroom/${classroom.id}`}
            basic
            color="teal"
          >
            Edit
          </Button>
          <Button
            onClick={() => handleDelete(classroom.id!)}
            basic
            color="red"
            loading={isSubmitting}
          >
            Delete
          </Button>
        </div>
      </Card.Content>
    </Card>
  );
};

export default observer(ClassroomDetail);
