import React, { useContext, useState, FC, useEffect } from 'react';
import { Form, Button, Grid, Label } from 'semantic-ui-react';
import { Form as FinalForm, Field } from 'react-final-form';
import { IClassroom } from '../../models/classroom';
import { RouteComponentProps, Link } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import TextInput from '../custom/TextInput';
import { combineValidators, isRequired } from 'revalidate';
import RootStore from '../../stores/RootStore';
import { FORM_ERROR } from 'final-form';

const validator = combineValidators({
  name: isRequired('name')
});
const ClassroomForm: FC<RouteComponentProps<{ id?: string }>> = ({
  history,
  match
}) => {
  const { classroomStore } = useContext(RootStore);
  const {
    loadClassroom,
    createClassroom,
    updateClassroom,
    isLoading
  } = classroomStore;
  const [classroom, setClassroom] = useState<IClassroom>({
    name: ''
  });
  const handleFormSubmit = async (classroom: IClassroom) => {
    try {
      if (!classroom.id) {
        await createClassroom(classroom);
      } else {
        await updateClassroom(classroom);
      }
      history.push('/classrooms');
    } catch (e) {
      return { [FORM_ERROR]: e.data.message };
    }
  };
  useEffect(() => {
    if (match.params.id) {
      loadClassroom(match.params.id).then(classroom => {
        classroom && setClassroom(classroom);
      });
    }
  }, [loadClassroom, match.params.id]);
  return (
    <FinalForm
      validate={validator}
      initialValues={classroom}
      onSubmit={handleFormSubmit}
      render={({
        handleSubmit,
        invalid,
        pristine,
        submitting,
        submitError,
        dirtySinceLastSubmit,
        hasValidationErrors
      }) => (
        <Grid>
          <Grid.Column width={10}>
            <Form
              className="fluid segment"
              onSubmit={handleSubmit}
              loading={isLoading}
            >
              <Form.Group grouped>
                <Field
                  name="name"
                  label="Classroom Name"
                  type="text"
                  placeholder="Classroom Name"
                  component={TextInput}
                />
                {submitError && !dirtySinceLastSubmit && (
                  <Label basic color="red" content={submitError} />
                )}
              </Form.Group>
              <Form.Group grouped>
                <Button
                  type="submit"
                  color="teal"
                  loading={submitting}
                  disabled={
                    pristine ||
                    submitting ||
                    hasValidationErrors ||
                    (submitError && !dirtySinceLastSubmit)
                  }
                >
                  Submit
                </Button>
                <Button as={Link} to="/classrooms">
                  Cancel
                </Button>
              </Form.Group>
            </Form>
          </Grid.Column>
        </Grid>
      )}
    />
  );
};

export default observer(ClassroomForm);
