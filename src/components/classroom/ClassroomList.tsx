import React, { useEffect, useContext, Fragment, useState, FC } from 'react';
import { Card, Grid, Confirm } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import ClassroomDetail from './ClassroomDetail';
import RootStore from '../../stores/RootStore';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { IClassroom } from '../../models/classroom';
import DimLoader from '../loader/DimLoader';

const ClassroomList: FC<RouteComponentProps> = ({ history }) => {
  const { classroomStore } = useContext(RootStore);
  const {
    loadClassroomsByTeacher,
    deleteClassroom,
    isSubmitting,
    isLoading
  } = classroomStore;
  const [isOpen, setOpen] = useState(false);
  const [classrooms, setClassrooms] = useState<IClassroom[]>([]);
  const [deleteId, setDeleteId] = useState('');
  useEffect(() => {
    loadClassroomsByTeacher().then(classrooms => {
      classrooms && setClassrooms(classrooms);
    });
  }, [loadClassroomsByTeacher]);
  const handleDelete = (id: string) => {
    setDeleteId(id);
    setOpen(true);
  };
  const handleConfirm = async () => {
    await deleteClassroom(deleteId);
    setOpen(false);
    history.push('/');
  };
  if (isLoading) {
    return <DimLoader />;
  }
  return (
    <Fragment>
      <Grid textAlign="center">
        <Grid.Column>
          <Card.Group stackable>
            {classrooms.map(classroom => (
              <ClassroomDetail
                key={classroom.id}
                classroom={classroom}
                handleDelete={handleDelete}
                isSubmitting={isSubmitting}
              />
            ))}
          </Card.Group>
        </Grid.Column>
      </Grid>
      <Confirm
        open={isOpen}
        onCancel={() => setOpen(false)}
        onConfirm={() => handleConfirm()}
      />
    </Fragment>
  );
};

export default withRouter(observer(ClassroomList));
