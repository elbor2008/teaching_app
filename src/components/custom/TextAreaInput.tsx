import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { Form, FormFieldProps } from 'semantic-ui-react';

interface Iprops
  extends FieldRenderProps<string, HTMLTextAreaElement>,
    FormFieldProps {}
const TextAreaInput: FC<Iprops> = ({
  input,
  placeholder,
  label,
  rows,
  meta: { error, touched }
}) => {
  return (
    <Form.Field>
      <label>{label}</label>
      <textarea {...input} placeholder={placeholder} rows={rows} />
    </Form.Field>
  );
};

export default TextAreaInput;
