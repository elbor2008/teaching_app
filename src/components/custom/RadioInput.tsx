import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { FormFieldProps } from 'semantic-ui-react';

interface Iprops
  extends FieldRenderProps<string, HTMLInputElement>,
    FormFieldProps {}
const RadioInput: FC<Iprops> = ({ input, label, disabled }) => {
  return (
    <div className="ui radio checkbox">
      <input
        type={input.type}
        name={input.name}
        onChange={input.onChange}
        checked={input.checked}
        value={input.value}
        disabled={disabled}
      />
      <label>{label}</label>
    </div>
  );
};

export default RadioInput;
