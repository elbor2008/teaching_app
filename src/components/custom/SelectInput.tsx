import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { Form, FormFieldProps, Select } from 'semantic-ui-react';

interface Iprops
  extends FieldRenderProps<string, HTMLSelectElement>,
    FormFieldProps {}
const SelectInput: FC<Iprops> = ({ input, options, label }) => {
  return (
    <Form.Field>
      <label>{label}</label>
      <Select
        options={options}
        value={input.value}
        onChange={(e, data) => input.onChange(data.value)}
      />
    </Form.Field>
  );
};

export default SelectInput;
