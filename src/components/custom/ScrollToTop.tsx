import { useEffect, FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

const ScrollToTop: FC<RouteComponentProps> = ({ location }) => {
  const { pathname } = location;

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};

export default withRouter(ScrollToTop);
