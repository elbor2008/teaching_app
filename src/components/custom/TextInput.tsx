import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { Form, FormFieldProps, Label } from 'semantic-ui-react';

interface Iprops
  extends FieldRenderProps<string, HTMLInputElement>,
    FormFieldProps {}
const TextInput: FC<Iprops> = ({
  input,
  type,
  placeholder,
  label,
  readOnly,
  meta: { error, touched }
}) => {
  return (
    <Form.Field error={touched && !!error} type={type}>
      <label>{label}</label>
      <input {...input} placeholder={placeholder} readOnly={readOnly} />
      {touched && error && (
        <Label basic color="red">
          {error}
        </Label>
      )}
    </Form.Field>
  );
};

export default TextInput;
