import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { Form, FormFieldProps, Label } from 'semantic-ui-react';
import DatePicker from 'react-datepicker';

interface Iprops
  extends FieldRenderProps<Date, HTMLInputElement>,
    FormFieldProps {}
const DateInput: FC<Iprops> = ({
  input,
  label,
  withPortal,
  readOnly,
  meta: { error }
}) => {
  return (
    <Form.Field error={!!error}>
      <label>{label}</label>
      <DatePicker
        selected={input.value}
        onChange={input.onChange}
        withPortal={withPortal}
        readOnly={readOnly}
        dateFormat="dd/MM/yyyy"
      />
      {error && (
        <Label basic color="red">
          {error}
        </Label>
      )}
    </Form.Field>
  );
};

export default DateInput;
