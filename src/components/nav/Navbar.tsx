import React, { FC, useContext } from 'react';
import { Menu, Container, Dropdown, Responsive } from 'semantic-ui-react';
import {
  NavLink,
  Link,
  RouteComponentProps,
  withRouter
} from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import RootStore from '../../stores/RootStore';
import { Role } from '../../models/role';

const Navbar: FC<RouteComponentProps> = ({ history }) => {
  const { userStore, authStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  const { clearUser } = authStore;
  const handleSignup = () => {
    clearUser();
    history.push('/signUp');
  };
  const handleSignout = () => {
    clearUser();
    history.push('/signIn');
  };
  return (
    <Menu inverted fixed="top" color="teal">
      <Container>
        <Menu.Item as={NavLink} to="/" exact>
          Home
        </Menu.Item>
        {selectedUser && selectedUser.role === Role.Teacher && (
          <Responsive minWidth={Responsive.onlyMobile.maxWidth}>
            <Menu.Item as={NavLink} to="/courses">
              Courses
            </Menu.Item>
          </Responsive>
        )}
        {selectedUser && selectedUser.role === Role.Teacher && (
          <Responsive minWidth={Responsive.onlyMobile.maxWidth}>
            <Menu.Item as={NavLink} to="/classrooms">
              Classrooms
            </Menu.Item>
          </Responsive>
        )}
        <Menu.Menu position="right">
          <Responsive maxWidth={Responsive.onlyMobile.maxWidth}>
            <Dropdown
              item
              pointing
              text={selectedUser ? 'Hi, ' + selectedUser.name : 'More'}
            >
              <Dropdown.Menu>
                {!selectedUser && (
                  <Dropdown.Item
                    onClick={handleSignup}
                    icon="signup"
                    text="Sign Up"
                  />
                )}
                {selectedUser && (
                  <Dropdown.Item
                    as={Link}
                    to="/SignUp"
                    icon="user circle outline"
                    text="Profile"
                  />
                )}
                {selectedUser && selectedUser.role === Role.Teacher && (
                  <Dropdown.Item
                    as={Link}
                    to="/courses"
                    text="Courses"
                    icon="paper plane"
                  />
                )}
                {selectedUser && selectedUser.role === Role.Teacher && (
                  <Dropdown.Item
                    as={Link}
                    to="/classrooms"
                    text="Classrooms"
                    icon="users"
                  />
                )}
                <Dropdown.Item
                  as={Link}
                  to="/signIn"
                  icon="sign-in"
                  text="Sign In"
                />
                {selectedUser && (
                  <Dropdown.Item
                    onClick={handleSignout}
                    icon="sign-out"
                    text="Sign Out"
                  />
                )}
              </Dropdown.Menu>
            </Dropdown>
          </Responsive>
          <Responsive minWidth={Responsive.onlyMobile.maxWidth}>
            <Dropdown
              item
              pointing
              text={selectedUser ? 'Hi, ' + selectedUser.name : 'More'}
            >
              <Dropdown.Menu>
                {!selectedUser && (
                  <Dropdown.Item
                    onClick={handleSignup}
                    icon="signup"
                    text="Sign Up"
                  />
                )}
                {selectedUser && (
                  <Dropdown.Item
                    as={Link}
                    to="/SignUp"
                    icon="user circle outline"
                    text="Profile"
                  />
                )}
                <Dropdown.Item
                  as={Link}
                  to="/signIn"
                  icon="sign-in"
                  text="Sign In"
                />
                {selectedUser && (
                  <Dropdown.Item
                    onClick={handleSignout}
                    icon="sign-out"
                    text="Sign Out"
                  />
                )}
              </Dropdown.Menu>
            </Dropdown>
          </Responsive>
        </Menu.Menu>
      </Container>
    </Menu>
  );
};

export default withRouter(observer(Navbar));
