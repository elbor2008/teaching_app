import React, { useContext, Fragment, FC } from 'react';
import {
  Grid,
  Header,
  Form,
  Segment,
  Button,
  Message,
  Label
} from 'semantic-ui-react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Form as FinalForm, Field } from 'react-final-form';
import { observer } from 'mobx-react-lite';
import { combineValidators, isRequired, composeValidators } from 'revalidate';
import { isEmail } from '../../validators/emailValidator';
import RootStore from '../../stores/RootStore';
import { FORM_ERROR } from 'final-form';

const validator = combineValidators({
  email: composeValidators(isRequired, isEmail)('email'),
  password: isRequired('password')
});
const SignIn: FC<RouteComponentProps> = ({ history, location }) => {
  const { authStore } = useContext(RootStore);
  const { signInUser } = authStore;
  const handleFormSubmit = async (user: {
    email: string;
    password: string;
  }) => {
    try {
      await signInUser(user.email, user.password);
      if (location.state) {
        location.state.from && history.push(location.state.from);
      } else {
        history.push('/');
      }
    } catch (e) {
      if (e) {
        return { [FORM_ERROR]: e.data.message };
      }
    }
  };
  return (
    <Grid verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450, margin: '0 auto' }}>
        <Header as="h2" color="teal" textAlign="center">
          Log-in to your account
        </Header>
        <FinalForm
          validate={validator}
          onSubmit={handleFormSubmit}
          render={({
            handleSubmit,
            submitting,
            pristine,
            submitError,
            dirtySinceLastSubmit,
            hasValidationErrors
          }) => (
            <Form size="large" onSubmit={handleSubmit}>
              <Segment stacked>
                <Field
                  name="email"
                  render={({ input, meta: { touched, error } }) => (
                    <Fragment>
                      {touched && error && (
                        <Label basic color="red">
                          {error}
                        </Label>
                      )}
                      <Form.Input
                        error={touched && !!error}
                        {...input}
                        type="text"
                        icon="user"
                        iconPosition="left"
                        placeholder="E-mail address"
                      />
                    </Fragment>
                  )}
                />
                <Field
                  name="password"
                  render={({ input, meta: { touched, error } }) => (
                    <Fragment>
                      {touched && error && (
                        <Label basic color="red">
                          {error}
                        </Label>
                      )}
                      <Form.Input
                        error={touched && !!error}
                        {...input}
                        icon="lock"
                        iconPosition="left"
                        placeholder="Password"
                        type="password"
                      />
                    </Fragment>
                  )}
                />
                {submitError && !dirtySinceLastSubmit && (
                  <Label basic color="red" content={submitError} />
                )}
                <Button
                  color="teal"
                  fluid
                  size="large"
                  loading={submitting}
                  disabled={
                    pristine ||
                    submitting ||
                    hasValidationErrors ||
                    (submitError && !dirtySinceLastSubmit)
                  }
                >
                  Login
                </Button>
              </Segment>
            </Form>
          )}
        />
        <Message>
          New to us? <Link to="/signUp">Sign Up</Link>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

export default observer(SignIn);
