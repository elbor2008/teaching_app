import { Form, Button, Label } from 'semantic-ui-react';
import React, { useState, useContext, FC, useEffect } from 'react';
import { Form as FinalForm, Field } from 'react-final-form';
import { observer } from 'mobx-react-lite';
import { IUser } from '../../models/user';
import { Role } from '../../models/role';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import TextInput from '../custom/TextInput';
import TextAreaInput from '../custom/TextAreaInput';
import RadioInput from '../custom/RadioInput';
import { combineValidators, composeValidators, isRequired } from 'revalidate';
import { isEmail } from '../../validators/emailValidator';
import { isAuMobile } from '../../validators/AuMobileValidator';
import RootStore from '../../stores/RootStore';
import { FORM_ERROR } from 'final-form';

const validator = combineValidators({
  name: isRequired('name'),
  password: isRequired('password'),
  email: composeValidators(isRequired, isEmail)('email'),
  mobile: composeValidators(isRequired, isAuMobile)('mobile')
});
const UserForm: FC<RouteComponentProps> = ({ history }) => {
  const { userStore } = useContext(RootStore);
  const { selectedUser, createUser, updateUser } = userStore;
  const [user, setUser] = useState<IUser>({
    name: '',
    password: '',
    title: '',
    email: '',
    mobile: '',
    city: '',
    introduction: '',
    role: Role.Teacher
  });

  const handleFormSubmit = async (user: IUser) => {
    try {
      if (user.id) {
        await updateUser(user);
        history.push('/');
      } else {
        await createUser(user);
        history.push('/signIn');
      }
    } catch (e) {
      if (e) {
        return { [FORM_ERROR]: e.data.message };
      }
    }
  };
  useEffect(() => {
    if (selectedUser) {
      selectedUser.password = '';
      setUser(selectedUser);
    }
  }, [selectedUser]);
  return (
    <FinalForm
      validate={validator}
      initialValues={user}
      onSubmit={handleFormSubmit}
      render={({
        handleSubmit,
        pristine,
        submitting,
        submitError,
        dirtySinceLastSubmit,
        hasValidationErrors
      }) => (
        <Form className="fluid segment" onSubmit={handleSubmit}>
          <Form.Group widths={3}>
            <Field
              name="name"
              label="NAME"
              type="text"
              placeholder="Name"
              component={TextInput}
            />
            <Field
              name="password"
              label="PASSWORD"
              type="password"
              placeholder="Password"
              component={TextInput}
            />
            <Field
              name="email"
              label="EMAIL"
              type="text"
              placeholder="Email"
              component={TextInput}
            />
          </Form.Group>
          <Form.Group widths={3}>
            <Field
              name="mobile"
              label="MOBILE"
              type="text"
              placeholder="Mobile"
              component={TextInput}
            />
            <Field
              name="title"
              label="TITLE"
              type="text"
              placeholder="Title"
              component={TextInput}
            />
            <Field
              name="city"
              label="CITY"
              type="text"
              placeholder="City"
              component={TextInput}
            />
          </Form.Group>
          <Form.Group grouped>
            <Field
              name="introduction"
              label="INTRODUCTION"
              placeholder="Introduction"
              component={TextAreaInput}
              rows={2}
            />
          </Form.Group>
          <Form.Group>
            <Form.Field>{selectedUser ? '' : 'Sign up as'}</Form.Field>
            <Form.Field>
              <Field
                type="radio"
                name="role"
                label="teacher"
                value={Role.Teacher}
                component={RadioInput}
                disabled={!!selectedUser}
              />
            </Form.Field>
            <Form.Field>
              <Field
                type="radio"
                name="role"
                label="student"
                value={Role.Student}
                component={RadioInput}
                disabled={!!selectedUser}
              />
            </Form.Field>
          </Form.Group>
          <Button
            loading={submitting}
            type="submit"
            color="teal"
            disabled={
              pristine ||
              submitting ||
              hasValidationErrors ||
              (submitError && !dirtySinceLastSubmit)
            }
          >
            Submit
          </Button>
          {submitError && !dirtySinceLastSubmit && (
            <Label basic color="red" content={submitError} />
          )}
        </Form>
      )}
    />
  );
};

export default withRouter(observer(UserForm));
