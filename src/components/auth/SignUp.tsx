import React, { Fragment, useContext } from 'react';
import { Message, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import UserForm from './UserForm';
import RootStore from '../../stores/RootStore';

const SignUp = () => {
  const { userStore } = useContext(RootStore);
  const { selectedUser } = userStore;
  return (
    <Fragment>
      <Message
        attached
        header={!selectedUser ? 'Welcome to our site!' : 'Welcome back!'}
        content={
          !selectedUser
            ? 'Fill out the form below to sign-up for a new account'
            : 'Fill out the form below to update for your account'
        }
      />
      <UserForm />
      {!selectedUser && (
        <Message attached="bottom" warning>
          <Icon name="help" />
          Already signed up?&nbsp;<Link to="/signIn">Login here</Link>
          &nbsp;instead.
        </Message>
      )}
    </Fragment>
  );
};

export default SignUp;
