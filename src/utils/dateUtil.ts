import moment from 'moment';

export const formatDate = (date: Date) => {
  return moment(date).format('DD/MM/YYYY');
};
export const addWeeks = (date: Date, weeks: number) => {
  return moment(date)
    .add(weeks, 'weeks')
    .toDate();
};
export const getDayOfWeek = (day: string) => {
  switch (day) {
    case '1':
      return 'Monday';
    case '2':
      return 'Tuesday';
    case '3':
      return 'Wednesday';
    case '4':
      return 'Thursday';
    case '5':
      return 'Friday';
    default:
      return 'Monday';
  }
};
