import axios, { AxiosResponse, AxiosError } from 'axios';
import { IUser } from '../models/user';
import { IClassroom } from '../models/classroom';
import { ICourse } from '../models/course';
import { history } from '..';
import { IClazz } from '../models/clazz';

// axios.defaults.baseURL = 'http://localhost:2000/api';
axios.defaults.baseURL =
  'http://teachingapi-env.ney3rmgfs4.ap-southeast-2.elasticbeanstalk.com/api';
axios.interceptors.request.use(
  config => {
    if (localStorage.getItem('jwt')) {
      config.headers['Authorization'] = `Bearer ${localStorage.getItem('jwt')}`;
    }
    return config;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(undefined, error => {
  if (error.message && error.message === 'Network Error') {
    history.push('/error');
  } else {
    const { status } = error.response;
    if (status === 401 || status === 403) {
      history.push('/signIn');
    } else if (status === 500) {
      history.push('/notfound');
    }
  }
  throw error.response;
});

const request = {
  get: (url: string) => axios.get(url).then((res: AxiosResponse) => res.data),
  post: (url: string, body: {}) =>
    axios.post(url, body).then((res: AxiosResponse) => res.data),
  put: (url: string, body: {}) =>
    axios.put(url, body).then((res: AxiosResponse) => res.data),
  delete: (url: string) =>
    axios.delete(url).then((res: AxiosResponse) => res.data)
};

const AuthAgent = {
  signIn: (
    email: string,
    password: string
  ): Promise<{ user: IUser; token: string }> =>
    request.get(`/auth/email/${email}/password/${password}`),
  signInByName: (name: string): Promise<{ user: IUser; token: string }> =>
    request.get(`/auth/name/${name}`)
};
const UserAgent = {
  list: (): Promise<IUser[]> => request.get('/users'),
  detail: (id: string): Promise<IUser> => request.get(`/users/${id}`),
  create: (user: IUser): Promise<IUser> => request.post('/users', user),
  update: (id: string, user: IUser): Promise<IUser> =>
    request.put(`/users/${id}`, user),
  delete: (id: string): Promise<IUser> => request.delete(`/users/${id}`),
  isNameExisting: (name: string): Promise<boolean> =>
    request.get(`/users/name/${name}`),
  isOtherNameExisting: (id: string, name: string): Promise<boolean> =>
    request.get(`/users/${id}/name/${name}`),
  isEmailExisting: (email: string): Promise<boolean> =>
    request.get(`/users/email/${email}`),
  isOtherEmailExisting: (id: string, email: string): Promise<boolean> =>
    request.get(`/users/${id}/email/${email}`)
};
const ClassroomAgent = {
  list: (): Promise<IClassroom[]> => request.get('/classrooms'),
  listByTeacher: (teacherId: string): Promise<IClassroom[]> =>
    request.get(`/classrooms/teacher/${teacherId}`),
  detail: (id: string): Promise<IClassroom> => request.get(`/classrooms/${id}`),
  create: (classroom: IClassroom): Promise<IClassroom> =>
    request.post('/classrooms', classroom),
  update: (id: string, classroom: IClassroom): Promise<IClassroom> =>
    request.put(`/classrooms/${id}`, classroom),
  delete: (id: string): Promise<IClassroom> =>
    request.delete(`/classrooms/${id}`),
  isNameExisting: (name: string): Promise<boolean> =>
    request.get(`/classrooms/name/${name}`),
  isOtherNameExisting: (id: string, name: string): Promise<boolean> =>
    request.get(`/classrooms/${id}/name/${name}`)
};
const CourseAgent = {
  list: (): Promise<ICourse[]> => request.get('/courses'),
  listByTeacher: (
    teacherId: string
  ): Promise<{
    courses: ICourse[];
    hasMore: boolean;
  }> => request.get(`/courses/teacher/${teacherId}`),
  listMoreByTeacher: (
    teacherId: string,
    page: number
  ): Promise<{
    courses: ICourse[];
    hasMore: boolean;
  }> => request.get(`/courses/teacher/${teacherId}/page/${page}`),
  listWithClazzs: (): Promise<{
    courses: ICourse[];
    hasMore: boolean;
  }> => request.get('/courses/withClazzs'),
  listMoreWithClazzs: (
    page: number
  ): Promise<{
    courses: ICourse[];
    hasMore: boolean;
  }> => request.get(`/courses/withClazzs/page/${page}`),
  create: (course: ICourse): Promise<ICourse> =>
    request.post('/courses', course),
  addClazz: (id: string, clazzId: string): Promise<ICourse> =>
    request.put(`/courses/${id}/clazz/${clazzId}`, {}),
  isNameExisting: (name: string): Promise<boolean> =>
    request.get(`/courses/name/${name}`),
  isOtherNameExisting: (id: string, name: string): Promise<boolean> =>
    request.get(`/courses/${id}/name/${name}`),
  isCourseCodeExisting: (courseCode: string): Promise<boolean> =>
    request.get(`/courses/courseCode/${courseCode}`),
  isOtherCourseCodeExisting: (
    id: string,
    courseCode: string
  ): Promise<boolean> => request.get(`/courses/${id}/courseCode/${courseCode}`),
  update: (id: string, course: ICourse): Promise<ICourse> =>
    request.put(`/courses/${id}`, course),
  detail: (id: string): Promise<ICourse> => request.get(`/courses/${id}`),
  delete: (id: string): Promise<ICourse> => request.delete(`/courses/${id}`)
};
const ClazzAgent = {
  create: (clazz: IClazz): Promise<IClazz> => request.post('/clazzs', clazz),
  getDaysOfWeek: (teacherId: string): Promise<string[]> =>
    request.get(`/clazzs/teacher/${teacherId}`),
  getClassroomsByDayOfWeek: (dayOfWeek: string): Promise<string[]> =>
    request.get(`/clazzs/dayOfWeek/${dayOfWeek}`),
  getClassroomsAndDaysOfWeek: (
    studentId: string,
    courseId: string
  ): Promise<{
    [key: string]: { classroomName: string; daysOfWeek: string[] };
  }> => request.get(`/clazzs/student/${studentId}/course/${courseId}`),
  addStudent: (id: string, studentId: string): Promise<IClazz> =>
    request.put(`/clazzs/${id}/student/${studentId}`, {}),
  removeStudent: (id: string, studentId: string): Promise<IClazz> =>
    request.delete(`/clazzs/${id}/student/${studentId}`)
};

export { AuthAgent, UserAgent, ClassroomAgent, CourseAgent, ClazzAgent };
