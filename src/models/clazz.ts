import { IUser } from './user';
import { IClassroom } from './classroom';
import { ICourse } from './course';

export interface IClazz {
  id?: string;
  dayOfWeek: string;
  course: string | ICourse;
  students?: string[] | IUser[];
  teacher: string | IUser;
  classroom: string | IClassroom;
}
