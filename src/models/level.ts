export enum Level {
  entry = 'entry',
  intermediate = 'intermediate',
  advanced = 'advanced'
}
