import { Length } from './length';
import { Level } from './level';
import { IUser } from './user';
import { Method } from './method';
import { Tuition } from './tuition';
import { IClazz } from './clazz';
export interface ICourse {
  id?: string;
  name: string;
  courseCode: string;
  prerequisite: string;
  length: Length; // (short: 4 weeks, normal: 30 weeks)
  method: Method; // (1: lecture, 2. turorial, 3: workshop)
  objective: string;
  level: Level; // (1: entry, 2: intermediate, 3: advanced)
  commenceDate: Date;
  completeDate: Date;
  tuition: Tuition; // (low: 1000, normal: 2000, high: 3000)
  teacher: string | IUser;
  clazz?: string | IClazz;
}
