import { ICourse } from './course';
import { Role } from './role';

export interface IUser {
  id?: string;
  name: string;
  password: string;
  title: string;
  email: string;
  mobile: string;
  city: string;
  introduction: string;
  role: Role; // (1:admin,2:teacher,3:student)
  courses?: string[] | ICourse[];
}
