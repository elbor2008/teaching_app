export interface IClassroom {
  id?: string;
  name: string;
  teacherId?: string;
}
