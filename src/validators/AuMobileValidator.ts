import { createValidator } from 'revalidate';

export const isAuMobile = createValidator(
  message => value => {
    if (
      value &&
      !/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(|-){0,1}[0-9]{2}(|-){0,1}[0-9]{2}(|-){0,1}[0-9]{1}(|-){0,1}[0-9]{3}$/.test(
        value
      )
    ) {
      return message;
    }
  },
  field => `${field} must be correct format in Australia`
);
