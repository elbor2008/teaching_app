import { createValidator } from 'revalidate';

export const isEmail = createValidator(
  message => value => {
    if (value && !/\S+@\S+\.\S+/.test(value)) {
      return message;
    }
  },
  field => `${field} must be correct format`
);
